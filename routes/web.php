<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use Illuminate\Http\Request;
Route::get('/', function () {
    return view('welcome');
});

Route::get('/create', function () {
    return view('grade/create');
});

Route::post('/process', function (Request $request) {
    $msg=$name=$grade='';
    if(strlen($request->name)>0 || strlen($request->mark)>0) {
        if (strlen(!$request->name) > 0) {
            $msg = "Name field is Required";
        } elseif (strlen(!$request->mark) > 0) {
            $msg = "Marks field is Required";
        } else {
            $name = $request->name;
            $mark = $request->mark;
            if ($mark >= 80 && $mark <= 100) {
                $grade = "A+";
            }
            if ($mark >= 70 && $mark < 80) {
                $grade = "A";
            }
            if ($mark >= 60 && $mark < 70) {
                $grade = "A-";
            }
            if ($mark >= 50 && $mark < 60) {
                $grade = "B";
            }
            if ($mark >= 40 && $mark < 50) {
                $grade = "C";
            }
            if ($mark >= 33 && $mark < 40) {
                $grade = "D";
            }
            if ($mark >= 0 && $mark < 33) {
                $grade = "F";
            }
            if ($mark < 0 || $mark > 100) {
                $msg = "Please Input Valid Number";
            }
        }
    }else{
        $msg="Name field and Mark field is required";
    }
    return view('grade.result',['name'=> $name, 'grade' => $grade,'msg' => $msg]);


});
Route::get('/blade', function () {
    return view('CleanBlog.home');
});
Route::get('/blade/about', function () {
    return view('CleanBlog.about');
});
Route::get('/blade/post', function () {
    return view('CleanBlog.blog');
});
Route::get('/blade/contact', function () {
    return view('CleanBlog.contact');
});