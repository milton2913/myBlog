<!doctype html>
<html>
    <head>
        <title>
            Grading System
        </title>
    </head>
    <body>
        <form action="/process" method="post">
            <label>Name :</label>
            <input type="text" name="name" placeholder="Type your name"/>
            <br/><br/>
            <label>Marks</label>
            <input type="number" name="mark" placeholder="type your mark (only integer)"/>
            <br/><br/>
            <input type="hidden" name="_token" value="<?php echo csrf_token()?>">
            <input type="submit" value="See Result">
        </form>
    </body>
</html>