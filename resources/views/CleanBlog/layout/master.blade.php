@include('CleanBlog.layout.head')
<body>

<!-- Navigation -->
@include('CleanBlog.layout.navigation')
<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header @yield('bg-class')">
    <div class="container">
        @yield('header-content')
    </div>
</header>

<!-- Main Content -->

@yield('container')

<hr>

<!-- Footer -->
@include('CleanBlog.layout.footer')